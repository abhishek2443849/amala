const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('bookings', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        subadmin_id:{
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        book_to_user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        type: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0 //	1=maid,2=chef,3=drivers,4=service provider // book to user id type
        },

        pay_amount_type: {    //1=available on order,2=available on office
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },

        amount: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ""
        },
        service_fee_amount: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ""
        },
        pending_amount: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ""
        },



        payment_method: {    // 0=pending , 1=complete
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        payment_type:{      //1=Available on order,2=Available at office
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        status: {                   //0=pending, 1=ongoing , 2=complete
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },

    }, {
        sequelize,
        tableName: 'bookings',
        timestamps: true,
        paranoid: true,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    { name: "id" },
                ]
            },
        ]
    });
};
