const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('subadmin_users', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    subadmin_id:{
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    usertype:{
      type: DataTypes.INTEGER, 	//1=maid,2=chef,3=driver
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    nationality: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue:""
    },
    age: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue:""
    },
    experience: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    salery: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    degree: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },

    height: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    weight: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
  
    availablity: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    price: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    passport:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    cv:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    license:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    status:{
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }

    
  }, {
    sequelize,
    tableName: 'subadmin_users',
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
