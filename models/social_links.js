const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('social_links', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    instagram_link:{
        type: DataTypes.STRING(255),
        allowNull: false,
        defaultValue: ""
    },
    tiktok_link:{
        type: DataTypes.STRING(255),
        allowNull: false,
        defaultValue: ""
    },
    twitter_link:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
  },
   
  }, {
    sequelize,
    tableName: 'social_links',
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
