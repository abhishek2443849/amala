var DataTypes = require("sequelize").DataTypes;
var _sub_admin = require("./sub_admin");

function initModels(sequelize) {
  var sub_admin = _sub_admin(sequelize, DataTypes);


  return {
    sub_admin,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
