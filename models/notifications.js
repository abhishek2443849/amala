const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('notifications', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    senderId:{
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    recieverId:{
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    message:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
  },
   
  }, {
    sequelize,
    tableName: 'notifications',
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
