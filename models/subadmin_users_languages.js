const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('subadmin_users_languages', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    subadmin_users_id:{
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    language_id:{
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
   
  }, {
    sequelize,
    tableName: 'subadmin_users_languages',
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
