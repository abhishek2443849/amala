const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('service_provider_service_types', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    service_provider_id:{
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    service_id:{
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
   
  }, {
    sequelize,
    tableName: 'service_provider_service_types',
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
