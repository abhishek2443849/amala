const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    username:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },

    country_code: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue:""
    },
    phone: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    phone_number: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: "0"
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    role: {
      type: DataTypes.ENUM('0','1','2','3'),
      allowNull: false,
      defaultValue: "0",
      comment: "0=>Admin,1=>User,2=>subadmin,3=>service provider"
    },
    otp: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    is_verify: {
      type: DataTypes.ENUM('0','1'),
      allowNull: false,
      defaultValue: "0",
      comment: "0=>no, 1=>yes"
    },
    is_private: {
      type: DataTypes.ENUM('0','1'),
      allowNull: false,
      defaultValue: "0",
      comment: "0=>public,1=>Private"
    },
    login_time: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: "0"
    },
    is_notification: {
      type: DataTypes.ENUM('0','1'),
      allowNull: false,
      defaultValue: "0",
      comment: "0=>on,1=>off"
    },
    hash_password: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    status: {
      type: DataTypes.ENUM('0','1'),
      allowNull: false,
      defaultValue: "1",
      comment: "0=>inactive 1=>Active"
    },
    service_provider_service_type_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    nationality: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    experience: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    address:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    salery:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    availablity:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    block:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    street:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    house:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    description:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    
    


    
  }, {
    sequelize,
    tableName: 'users',
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
