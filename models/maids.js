const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('maids', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    subadmin_id:{
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    nationality: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue:""
    },
    age: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue:""
    },
    experience: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    salery: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    degree: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },

    height: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    weight: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
  
    availablity: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    price: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    passport:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    cv:{
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }

    
  }, {
    sequelize,
    tableName: 'maids',
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
