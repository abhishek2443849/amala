const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('chef_languages', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    chef_id:{
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    language_id:{
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
   
  }, {
    sequelize,
    tableName: 'chef_languages',
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
