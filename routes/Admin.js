var express = require('express');
var router = express.Router();
const AuthControler = require('../Controllers/Auth');

const UserControler = require('../Controllers/Usercontroller');
const CmsControler = require('../Controllers/Cmscontroller');
const subadminCotroller = require('../Controllers/subadminController');
const serviceProviderCotroller = require('../Controllers/serviceProviderController');
const servicesController = require('../Controllers/servicesController');
const languagesController = require('../Controllers/languagesController');
const serviceFeesController = require('../Controllers/servicesFeesController');
const bookingController = require('../Controllers/bookingController');
const socailLinksController = require('../Controllers/socailLinksController');
const transactionsController = require('../Controllers/transactionsController');
const NotificationController=require('../Controllers/notificationController')
const contactUsController=require('../Controllers/contactUsController')
const subadminUsersController=require('../Controllers/subadminUsersController')
const withdrawalController=require('../Controllers/withdrawalController')





// Admin Auth //
router.get('/login', AuthControler.loginpage)
router.post('/login', AuthControler.login)
router.get('/dashboard', AuthControler.dashboard)
router.get('/profile', AuthControler.profile)
router.post('/profile', AuthControler.edit_profile)
router.get('/changepasswordpage', AuthControler.changepasswordpage)
router.post('/changepasswordpage', AuthControler.changepassword)
router.get('/logout', AuthControler.logout)


//bookings//
router.get('/admin_booking_listing', bookingController.admin_booking_listing)

//transactions//
router.get('/admin_transactions_listing', transactionsController.admin_transactions_listing)


// Users //
router.get('/userlisting', UserControler.userlisting)
router.get('/userview/:id', UserControler.userview)
router.post('/viewupdate/:id', UserControler.viewupdate)
router.post('/deleteduser', UserControler.deleteduser)
router.post('/user_status', UserControler.user_status)

// privacy policy // 
router.get('/privacy_policy', CmsControler.privacy_policy)
router.get('/aboutus', CmsControler.AboutUs)
router.get('/termsConditions', CmsControler.termsConditions)
router.post('/privacy_policy',CmsControler.privacy_policy_update)
router.post('/aboutus', CmsControler.AboutUs_update)
router.post('/termsConditions', CmsControler.terms_update)

//subadmin///
router.get('/add_subadmin',subadminCotroller.add_subadmin)
router.get("/check_email",subadminCotroller.check_email);
router.get("/check_username",subadminCotroller.check_username);
router.post('/subadminpost', subadminCotroller.subadminpost)
router.get('/subadminlisting',subadminCotroller.interestlisting)
router.post('/interest_deleted', subadminCotroller.interest_deleted)
router.post('/interest_status', subadminCotroller.interest_status)
router.get('/interests_view/:id', subadminCotroller.interests_view)
router.get('/interests_edit/:id', subadminCotroller.interests_edit)
router.post('/interest_update/:id', subadminCotroller.interest_update)


//service provider///
router.get('/add_service_provider',serviceProviderCotroller.add_service_provider)
router.post('/service_provider_post', serviceProviderCotroller.service_provider_post)
router.get("/check_serviceprovider_email",serviceProviderCotroller.check_serviceprovider_email);

router.get('/service_provider_listing',serviceProviderCotroller.service_provider_listing)
router.post('/service_provider_deleted', serviceProviderCotroller.service_provider_deleted)
router.post('/service_provider_status', serviceProviderCotroller.service_provider_status)
router.get('/service_provider_view/:id', serviceProviderCotroller.service_provider_view)
router.get('/service_provider_edit/:id', serviceProviderCotroller.service_provider_edit)
router.post('/service_provider_update/:id', serviceProviderCotroller.service_provider_update)

//services///
router.get('/add_services',servicesController.add_services)
router.post('/services_post', servicesController.services_post)
router.get('/services_listing',servicesController.services_listing)
router.post('/services_deleted', servicesController.services_deleted)
router.post('/services_status', servicesController.services_status)
router.get('/services_view/:id', servicesController.services_view)
router.get('/services_edit/:id', servicesController.services_edit)
router.post('/services_update/:id', servicesController.services_update)

//Languages///
router.get('/add_languages',languagesController.add_languages)
router.post('/languages_post', languagesController.languages_post)
router.get('/languages_listing',languagesController.languages_listing)
router.post('/languages_deleted', languagesController.languages_deleted)
router.post('/languages_status', languagesController.languages_status)
router.get('/languages_view/:id', languagesController.languages_view)
router.get('/languages_edit/:id', languagesController.languages_edit)
router.post('/languages_update/:id', languagesController.languages_update)

//Services Fees//

router.get('/add_services_fees', serviceFeesController.add_services_fees)
router.post('/services_fees_update', serviceFeesController.services_fees_update)

//Social links//

router.get('/add_socail_links', socailLinksController.add_socail_links)
router.post('/socail_links_update', socailLinksController.socail_links_update)

//Notifications//
router.get('/notification/users', NotificationController.notification)
router.post('/notification/users', NotificationController.store)

//Contact Us//
router.get('/contact_us', contactUsController.contactUs)



router.get('/maid_list',subadminUsersController.maid_listing)
router.get('/chef_list',subadminUsersController.chef_listing)
router.get('/driver_list',subadminUsersController.driver_listing)

router.get('/withdrawal_listing',withdrawalController.withdrawalRequest)
router.post('/withdrawl_accept_reject',withdrawalController.withdrawl_accept_reject)














module.exports = router;
