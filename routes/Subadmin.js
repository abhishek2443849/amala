var express = require('express');
var router = express.Router();
const SubaminAuthControler = require('../Controllers/subadminAuth');
const MaidControler = require('../Controllers/maidController');
const ChefControler = require('../Controllers/chefController');
const DriverControler = require('../Controllers/driverController');
const SubadminCmscontroller = require('../Controllers/subadminCmscontroller');
const bookingController = require('../Controllers/bookingController');
const multer = require('multer');
const XLSX = require('xlsx');
const path = require('path');
const driverController = require('../Controllers/driverController');
const NotificationController=require('../Controllers/notificationController')
const bankController=require('../Controllers/bankController')







// Admin Auth //
router.get('/login', SubaminAuthControler.loginpage)
router.post('/login', SubaminAuthControler.login)
router.get('/dashboard', SubaminAuthControler.dashboard)
router.get('/profile', SubaminAuthControler.profile)
router.post('/profile', SubaminAuthControler.edit_profile)
router.get('/changepasswordpage', SubaminAuthControler.changepasswordpage)
router.post('/changepasswordpage', SubaminAuthControler.changepassword)
router.get('/logout', SubaminAuthControler.logout)

//upload maid using excel//
router.post('/uploadMaid',MaidControler.uploadMaid)


//maid//
router.get('/add_maid',MaidControler.add_maid)
router.post('/maid_post', MaidControler.maid_post)
router.get('/maid_listing',MaidControler.maid_listing)
router.post('/maid_deleted', MaidControler.maid_deleted)
router.post('/maid_status', MaidControler.maid_status)
router.get('/maid_view/:id', MaidControler.maid_view)
router.get('/maid_edit/:id', MaidControler.maid_edit)
router.post('/maid_update/:id', MaidControler.maid_update)

//upload chef using excel//
router.post('/uploadChef',ChefControler.uploadChef)

//chef//
router.get('/add_chef',ChefControler.add_chef)
router.post('/chef_post', ChefControler.chef_post)
router.get('/chef_listing',ChefControler.chef_listing)
router.post('/chef_deleted', ChefControler.chef_deleted)
router.post('/chef_status', ChefControler.chef_status)
router.get('/chef_view/:id', ChefControler.chef_view)
router.get('/chef_edit/:id', ChefControler.chef_edit)
router.post('/chef_update/:id', ChefControler.chef_update)

//upload driver using excel//
router.post('/uploadDriver',driverController.uploadDriver)
//driver//
router.get('/add_driver',DriverControler.add_driver)
router.post('/driver_post', DriverControler.driver_post)
router.get('/driver_listing',DriverControler.driver_listing)
router.post('/driver_deleted', DriverControler.driver_deleted)
router.post('/driver_status', DriverControler.driver_status)
router.get('/driver_view/:id', DriverControler.driver_view)
router.get('/driver_edit/:id', DriverControler.driver_edit)
router.post('/driver_update/:id', DriverControler.driver_update)

// Bookings //
router.get('/subadmin_booking_listing', bookingController.subadmin_booking_listing)
router.post('/subadmin_status_accept_reject',bookingController.subadmin_status_accept_reject);

//Notifications//
router.get('/subadmin_notification', NotificationController.subadmin_notification)

//cms//
router.get('/privacy_policy', SubadminCmscontroller.privacy_policy)
router.get('/aboutus', SubadminCmscontroller.AboutUs)
router.get('/termsConditions', SubadminCmscontroller.termsConditions)
//bank//
router.get('/subadmin_add_bank', bankController.subadmin_add_bank)
router.post("/subadmin_post_bank",bankController.subadmin_post_bank);
router.post('/withdrawal_request',bankController.withdrawalRequest);













module.exports = router;
