var express = require('express');
var router = express.Router();
const ServiceProviderAuthControler = require('../Controllers/serviceProviderAuth');
const serviceProvidercmsController = require('../Controllers/serviceproviderCmscontroller');
const bookingController = require('../Controllers/bookingController');
const bankController=require('../Controllers/bankController')









// Admin Auth //
router.get('/login', ServiceProviderAuthControler.loginpage)
router.post('/login', ServiceProviderAuthControler.login)
router.get('/dashboard', ServiceProviderAuthControler.dashboard)
router.get('/profile', ServiceProviderAuthControler.profile)
router.post('/profile', ServiceProviderAuthControler.edit_profile)
router.get('/changepasswordpage', ServiceProviderAuthControler.changepasswordpage)
router.post('/changepasswordpage', ServiceProviderAuthControler.changepassword)
router.get('/logout', ServiceProviderAuthControler.logout)

// bookings //
router.get('/listing', bookingController.booking_listing)
router.post('/status_accept_reject',bookingController.status_accept_reject);



//cms//
router.get('/privacy_policy', serviceProvidercmsController.privacy_policy)
router.get('/aboutus', serviceProvidercmsController.AboutUs)
router.get('/termsConditions', serviceProvidercmsController.termsConditions)

//bank//
router.get('/serviceprovider_add_bank', bankController.serviceprovider_add_bank)
router.post("/serviceprovider_post_bank",bankController.serviceprovider_post_bank);
router.post('/serviceprovider_withdrawalRequest',bankController.serviceprovider_withdrawalRequest);











module.exports = router;
