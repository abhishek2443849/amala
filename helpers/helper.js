const path = require("path");
var uuid = require("uuid").v4;
module.exports = {
  fileUpload: async (file, folder) => {
    if (file) {
      var extension = path.extname(file.name);
      var filename = uuid() + extension;
      file.mv(
        process.cwd() + `/public/images/${folder}/` + filename,
        function (err) {
          if (err) return err;
        }
      );
    }
    // return filename;

    let fullpath = `/images/${folder}/` + filename
    return fullpath;
  },

  files_upload: async function(image,folderName){
    if (image) {
         var extension = path.extname(image.name);
         var filename = uuid() + extension;
         var sampleFile = image;
         sampleFile.mv(process.cwd() + `/public/images/${folderName}/` + filename, (err) => {
             if (err) throw err;
         });

         return filename;
     }

 },
 
  success: function (res, message, body = {}) {
    return res.status(200).json({
      success: 1,
      code: 200,
      message: message,
      body: body,
    });
  },
  error: function (res, err, body = {}) {
    let code = typeof err === "object" ? (err.code ? err.code : 400) : 400;
    let message =
      typeof err === "object" ? (err.message ? err.message : "") : err;
    res.status(code).json({
      success: false,
      code: code,
      message: message,
      body: body,
    });
  },
}