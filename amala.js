var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
// const db = require('./dbconnection/db')
var bodyParser = require('body-parser')
var fileUpload = require('express-fileupload')
let session = require('express-session');
const flash = require('express-flash');


var AdminRouter = require('./routes/Admin');
var subAdminRouter = require('./routes/Subadmin');
var serviceProviderRouter = require('./routes/ServiceProvider.js');


var usersRouter = require('./routes/users');
require('dotenv').config()
var app = express();
const PORT = process.env.PORT
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(fileUpload());
app.use(flash());
app.use(session({ secret: 'session' }));


app.use('/admin', AdminRouter);
app.use('/subadmin', subAdminRouter);
app.use('/serviceprovider', serviceProviderRouter);

app.use('/users', usersRouter);

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });
// db()
app.listen(PORT,(req,res)=>{
  console.log(`Your server start with port ${PORT}`);
})
