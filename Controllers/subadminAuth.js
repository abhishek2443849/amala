const helper = require("../helpers/helper");
const db = require("../models");
var CryptoJS = require("crypto-js");
const ENV = process.env;
const sequelize = require("sequelize");
const Op= sequelize.Op


var title = "dashboard";

module.exports = {
  // ALL RENDER PAGES //
  loginpage: async (req, res) => {
    try {
      res.render("subadmin/login");
    } catch (error) {
      return helper.error(res, error);
    }
  },
  login: async (req, res) => {
    try {
      console.log(req.body, "KKKKKKKKKKKKKKKKKKKKKKKK");
      const find_data = await db.users.findOne({
        // where: {
        //   email: req.body.email,
        //   deletedAt: null,
        //   role: "2",
        // },
        where: {
          [Op.or]: [
            { 
              email: req.body.identifier 
            },
            { 
              username: req.body.identifier
            }
          ],
          deletedAt: null,
          role: "2",
          // status:"1"
        },
        raw: true,
      });

      if (!find_data || find_data.status !== '1') {
        req.flash("error", "Sub admin is not active by admin");
        return res.redirect("/subadmin/login");
      }


      if (find_data == null) {
        req.flash("error", "Please enter valid email");
        res.redirect("/subadmin/login");
      } else {
        // Decrypt
        var bytes = CryptoJS.AES.decrypt(find_data.password, ENV.crypto_key);
        let Decrypt_data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        let check_password = Decrypt_data == req.body.password;
        let mode = "light";
        find_data.mode = mode;
        if (check_password == true) {
          req.session.subadmin = find_data;

          req.flash("success", " Your are login succesfully ");
          res.redirect("/subadmin/dashboard");
        } else {
          req.flash("error", "Please enter valid  password");
          res.redirect("/subadmin/login");
        }
      }
    } catch (error) {
      return helper.error(res, error);
    }
  },
  dashboard: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      let session = req.session.subadmin;

      const maidCount = await db.subadmin_users.count({
        where: {deletedAt: null,subadmin_id:req.session.subadmin.id,usertype:1},
      });

      const chefCount = await db.subadmin_users.count({
        where: {deletedAt: null,subadmin_id:req.session.subadmin.id,usertype:2 },
      });
      
      const driverCount = await db.subadmin_users.count({
        where: {deletedAt: null,subadmin_id:req.session.subadmin.id,usertype:3 },
      });

      const bookingsCount = await db.bookings.count({
        where: {subadmin_id:req.session.subadmin.id,deletedAt: null },
      });

      




      res.render("subadmin/dashboard", {
        session,
        maidCount: maidCount,chefCount,driverCount,bookingsCount,
        title,
      });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  profile: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      let session = req.session.subadmin;
      const profile = await db.users.findOne({
        where: {
          email: req.session.subadmin.email,
        },
      });
      res.render("subadmin/profile", { profile, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  edit_profile: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      let folder = "admin";
      if (req.files && req.files.image) {
        let images = await helper.fileUpload(req.files.image, folder);

        req.body.image = images;
      }

      const profile = await db.users.update(
        {
          name: req.body.name,
          image: req.body.image,
          address:req.body.address
        },
        {
          where: {
            id: req.session.subadmin.id,
          },
        }
      );
      req.flash("success", "Profile updated succesfully");
      res.redirect("/subadmin/dashboard");
    } catch (error) {
      return helper.error(res, error);
    }
  },
  changepasswordpage: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      let session = req.session.subadmin;
      res.render("subadmin/changepassword", { session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  changepassword: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      let session = req.session.subadmin;
      let findAdmin = await db.users.findOne({
        where: {
          id: session.id,
        },
      });
      let Encrypt_data = findAdmin.password;
      // Decrypt
      var bytes = CryptoJS.AES.decrypt(Encrypt_data, ENV.crypto_key);
      var Decrypt_data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      let check_old = Decrypt_data === +req.body.oldpassword;
      // Encrypt
      var data = +req.body.newpassword;
      var Newpassword = CryptoJS.AES.encrypt(
        JSON.stringify(data),
        ENV.crypto_key
      ).toString();
      if (check_old == true) {
        let update_password = await db.users.update(
          { password: Newpassword },
          {
            where: {
              id: session.id,
            },
          }
        );
        req.flash("success", "Update password succesfully");
        res.redirect("/subadmin/login");
      } else {
        req.flash("error", "Please enter valid old password");
        res.redirect("/subadmin/changepasswordpage");
      }
    } catch (error) {
      return helper.error(res, error);
    }
  },
  logout: async (req, res) => {
    try {
      req.session.destroy();
      res.redirect("/subadmin/login");
    } catch (error) {
      return helper.error(res, error);
    }
  },
};
