const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); ////////////////
const ENV = process.env;

var title = "services";
module.exports = {
  add_services: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;

      res.render("Admin/services/add", { session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  services_post: async (req, res) => {
    try {
   
      const add_inst = await db.services.create({
        name: req.body.name
      });

      req.flash('success', 'Services added succesfully')
      res.redirect("/admin/services_listing")
    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      res.status(500).send("Internal Server Error");
    }
  },

  services_listing: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      const find_user = await db.services.findAll({
        where:{
          deletedAt: null,

        },
        order: [['id', 'DESC']]

      });
      let session = req.session.admin;
      res.render("Admin/services/listing", { session, find_user, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  services_view: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      const data_view = await db.services.findOne({
        where: {
          id: req.params.id,
        },
      });
      res.render("Admin/services/view", { data_view, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  services_edit: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      const data_view = await db.services.findOne({
        where: {
          id: req.params.id,
        },
      });
      res.render("Admin/services/edit", { data_view, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  services_update: async (req, res) => {
    try {
   
      const updateUser = await db.services.update(
        req.body,

        {
          where: {
            id: req.params.id,
          },
        }
      );

      req.flash('success', 'Service updated succesfully')
      res.redirect("/admin/services_listing")
    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      // res.status(500).send("Internal Server Error");
    }
  },
  services_status: async (req, res) => {
    try {
      const find_status = await db.services.update(
        { status: req.body.value },
        {
          where: { id: req.body.id },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
  services_deleted: async (req, res) => {
    try {
      let data = Date.now();
      const userdelete = await db.services.update(
        { deletedAt: data },
        {
          where: {
            id: req.body.id,
          },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
};
