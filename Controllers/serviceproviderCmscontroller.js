
const helper = require('../helpers/helper')
var CryptoJS = require("crypto-js");
const db = require('../models')
const ENV = process.env

var title = "serviceprovidercms"
module.exports = {
    
    privacy_policy: async (req, res) => {
        try {
            if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
            const privacy_policy = await db.contents.findOne({
                where:{
                    accessor: "privacyPolicy"
                } })
            let session = req.session.serviceprovider

          
            res.render('serviceprovider/cms/privacypolicy', { session,title,privacy_policy})
        } catch (error) {
            return helper.error(res, error)
        }
    },
    AboutUs: async (req, res) => {
        try {
            if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
            let session = req.session.serviceprovider
            const aboutus = await db.contents.findOne({
                where:{
                    accessor: "aboutUs"
                }
            })
            res.render('serviceprovider/cms/aboutus', { session, title,aboutus })
        } catch (error) {
            return helper.error(res, error)

        }
    },
    termsConditions: async (req, res) => {
        try {
            if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
            let session = req.session.serviceprovider
            const termsCondition = await db.contents.findOne({
                where:{
                    accessor: "termsAndConditions"
                }
            })
            res.render('serviceprovider/cms/termsConditions', { session, title,termsCondition })
        } catch (error) {
            return helper.error(res, error)

        }
    },
 
}