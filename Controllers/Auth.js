const helper = require("../helpers/helper");
const db = require("../models");
var CryptoJS = require("crypto-js");
const ENV = process.env;

var title = "dashboard";

module.exports = {
  // ALL RENDER PAGES //
  loginpage: async (req, res) => {
    try {
      //        var data = 123456

      //       // Encrypt
      //        var Encrypt_data = CryptoJS.AES.encrypt(JSON.stringify(data), ENV.crypto_key).toString();

      //       // Decrypt
      //        var bytes  = CryptoJS.AES.decrypt(Encrypt_data, ENV.crypto_key);
      //        var Decrypt_data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      // console.log(Encrypt_data,"kkkkkk",Decrypt_data,"hgfhfghgf",bytes);

      res.render("Admin/login");
    } catch (error) {
      return helper.error(res, error);
    }
  },
  login: async (req, res) => {
    try {
      console.log(req.body, "KKKKKKKKKKKKKKKKKKKKKKKK");
      const find_data = await db.users.findOne({
        where: {
          email: req.body.email,
          deletedAt: null,
          role: "0",
        },
        raw: true,
      });

      if (find_data == null) {
        req.flash("error", "Please enter valid email");
        res.redirect("/admin/login");
      } else {
        // Decrypt
        var bytes = CryptoJS.AES.decrypt(find_data.password, ENV.crypto_key);
        let Decrypt_data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        let check_password = Decrypt_data == req.body.password;
        let mode = "light";
        find_data.mode = mode;
        if (check_password == true) {
          req.session.admin = find_data;

          req.flash("success", " Your are login succesfully ");
          res.redirect("/admin/dashboard");
        } else {
          req.flash("error", "Please enter valid  password");
          res.redirect("/admin/login");
        }
      }
    } catch (error) {
      return helper.error(res, error);
    }
  },
  dashboard: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;

      const userCount = await db.users.count({
        where: { role: "1", deletedAt: null },
      });

      const subadminCount = await db.users.count({
        where: { role: "2", deletedAt: null },
      });

      const serviceproviderCount = await db.users.count({
        where: { role: "3", deletedAt: null },
      });
      

      res.render("Admin/dashboard", {
        session,
        userCount: userCount,
        subadminCount,
        serviceproviderCount,
        title,
      });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  profile: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      const profile = await db.users.findOne({
        where: {
          email: req.session.admin.email,
        },
      });
      res.render("Admin/profile", { profile, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  edit_profile: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let folder = "admin";
      if (req.files && req.files.image) {
        let images = await helper.fileUpload(req.files.image, folder);

        req.body.image = images;
      }

      const profile = await db.users.update(
        {
          name: req.body.name,
          image: req.body.image,
        },
        {
          where: {
            id: req.session.admin.id,
          },
        }
      );
      req.flash("success", "Profile updated succesfully");
      res.redirect("/admin/dashboard");   
     } catch (error) {
      return helper.error(res, error);
    }
  },
  changepasswordpage: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      res.render("Admin/changepassword", { session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  changepassword: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      let findAdmin = await db.users.findOne({
        where: {
          id: session.id,
        },
      });
      let Encrypt_data = findAdmin.password;
      // Decrypt
      var bytes = CryptoJS.AES.decrypt(Encrypt_data, ENV.crypto_key);
      var Decrypt_data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      let check_old = Decrypt_data === +req.body.oldpassword;
      // Encrypt
      var data = +req.body.newpassword;
      var Newpassword = CryptoJS.AES.encrypt(
        JSON.stringify(data),
        ENV.crypto_key
      ).toString();
      if (check_old == true) {
        let update_password = await db.users.update(
          { password: Newpassword },
          {
            where: {
              id: session.id,
            },
          }
        );
        req.flash("success", "Update password succesfully");
        res.redirect("/admin/login");
      } else {
        req.flash("error", "Please enter valid old password");
        res.redirect("/admin/changepasswordpage");
      }
    } catch (error) {
      return helper.error(res, error);
    }
  },
  logout: async (req, res) => {
    try {
      req.session.destroy();
      res.redirect("/admin/login");
    } catch (error) {
      return helper.error(res, error);
    }
  },
};
