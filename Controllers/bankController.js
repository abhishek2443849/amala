const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); 
const ENV = process.env;
const sequelize=db.sequelize

var title = "bank";
module.exports = {
  subadmin_add_bank: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      let session = req.session.subadmin;
      const getBank = await db.bank.findAll({
        where: {
          added_by: req.session?.subadmin?.id || null
        },
        raw: true
      })
      const totalAmount = await db.bookings.findOne({
        attributes: [
          [sequelize.fn('SUM', sequelize.col('amount')), 'totalAmount']
        ],
        where: {
          subadmin_id: req.session?.subadmin?.id || null,
          payment_type:2
        },
        raw: true
      });

      res.render("subadmin/bank/bank", {getBank,totalAmount, session,title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  
  subadmin_post_bank: async (req, res) => {
    try {
      const addBank = await db.bank.create({
        added_by: req?.session?.subadmin?.id,
        name: req.body.name,
        bank_name: req.body.bank_name,
        bank_account: req.body.bank_account
      })
      
 
      req.flash('success', 'Bank account add successfully');
      res.redirect("/subadmin/subadmin_add_bank")

    } catch (error) {
      console.log(error, '===========error==================')
    }

  },
  withdrawalRequest: async (req, res) => {
    try {
      const withdrawalRequest = await db.withdrawal_requests.create({
        amount: req.body.amount,
        bank_id: req.body.bank_id,
        request_by: req?.session?.subadmin?.id
      })
      return res.status(200).json(withdrawalRequest)

    } catch (error) {
      console.log(error, '========error========')
    }
  },

  serviceprovider_add_bank: async (req, res) => {
    try {
      if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
      let session = req.session.serviceprovider;
      const getBank = await db.bank.findAll({
        where: {
          added_by: req.session?.serviceprovider?.id || null
        },
        raw: true
      })
      const totalAmount = await db.bookings.findOne({
        attributes: [
          [sequelize.fn('SUM', sequelize.col('amount')), 'totalAmount']
        ],
        where: {
          book_to_user_id: req.session?.subadmin?.id || null,
          payment_type:2,
          type:4
        },
        raw: true
      });

      res.render("serviceprovider/bank/bank", {getBank,totalAmount, session,title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  
  serviceprovider_post_bank: async (req, res) => {
    try {
      const addBank = await db.bank.create({
        added_by: req?.session?.serviceprovider?.id,
        name: req.body.name,
        bank_name: req.body.bank_name,
        bank_account: req.body.bank_account
      })
      
 
      req.flash('success', 'Bank account add successfully');
      res.redirect("/serviceprovider/serviceprovider_add_bank")

    } catch (error) {
      console.log(error, '===========error==================')
    }

  },
  serviceprovider_withdrawalRequest: async (req, res) => {
    try {
      const withdrawalRequest = await db.withdrawal_requests.create({
        amount: req.body.amount,
        bank_id: req.body.bank_id,
        request_by: req?.session?.serviceprovider?.id
      })
      return res.status(200).json(withdrawalRequest)

    } catch (error) {
      console.log(error, '========error========')
    }
  },



};
