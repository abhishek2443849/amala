const helper = require("../helpers/helper");
const db = require("../models");
var CryptoJS = require("crypto-js");
const ENV = process.env;

var title = "dashboard";

module.exports = {
  // ALL RENDER PAGES //
  loginpage: async (req, res) => {
    try {
      res.render("serviceprovider/login");
    } catch (error) {
      return helper.error(res, error);
    }
  },
  login: async (req, res) => {
    try {
      console.log(req.body, "KKKKKKKKKKKKKKKKKKKKKKKK");
      const find_data = await db.users.findOne({
        where: {
          email: req.body.email,
          deletedAt: null,
          role: "3",
        },
        raw: true,
      });

      if (!find_data || find_data.status !== '1') {
        req.flash("error", "Service Provider is not active by admin");
        return res.redirect("/serviceprovider/login");
      }
      

      if (find_data == null) {
        req.flash("error", "Please enter valid email");
        res.redirect("/serviceprovider/login");
      } else {
        // Decrypt
        var bytes = CryptoJS.AES.decrypt(find_data.password, ENV.crypto_key);
        let Decrypt_data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        let check_password = Decrypt_data == req.body.password;
        let mode = "light";
        find_data.mode = mode;
        if (check_password == true) {
          req.session.serviceprovider = find_data;

          req.flash("success", " Your are login succesfully ");
          res.redirect("/serviceprovider/dashboard");
        } else {
          req.flash("error", "Please enter valid  password");
          res.redirect("/serviceprovider/login");
        }
      }
    } catch (error) {
      return helper.error(res, error);
    }
  },
  dashboard: async (req, res) => {
    try {
      if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
      let session = req.session.serviceprovider;
      const bookingsCount = await db.bookings.count({
        where:{
          book_to_user_id: req.session.serviceprovider.id,
          type:4,
          deletedAt: null
        }
      });


      res.render("serviceprovider/dashboard", {
        session,
        bookingsCount,
        title,
      });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  profile: async (req, res) => {
    try {
      if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
      let session = req.session.serviceprovider;
      const profile = await db.users.findOne({
        where: {
          email: req.session.serviceprovider.email,
        },
      });
      res.render("serviceprovider/profile", { profile, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  edit_profile: async (req, res) => {
    try {
      if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
      let folder = "admin";
      if (req.files && req.files.image) {
        let images = await helper.fileUpload(req.files.image, folder);

        req.body.image = images;
      }

      const profile = await db.users.update(
        {
          name: req.body.name,
          image: req.body.image,
        },
        {
          where: {
            id: req.session.serviceprovider.id,
          },
        }
      );
      req.flash("success", "Profile updated succesfully");
      res.redirect("/serviceprovider/dashboard");
    } catch (error) {
      return helper.error(res, error);
    }
  },
  changepasswordpage: async (req, res) => {
    try {
      if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
      let session = req.session.serviceprovider;
      res.render("serviceprovider/changepassword", { session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  changepassword: async (req, res) => {
    try {
      if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
      let session = req.session.serviceprovider;
      let findAdmin = await db.users.findOne({
        where: {
          id: session.id,
        },
      });
      let Encrypt_data = findAdmin.password;
      // Decrypt
      var bytes = CryptoJS.AES.decrypt(Encrypt_data, ENV.crypto_key);
      var Decrypt_data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      let check_old = Decrypt_data === +req.body.oldpassword;
      // Encrypt
      var data = +req.body.newpassword;
      var Newpassword = CryptoJS.AES.encrypt(
        JSON.stringify(data),
        ENV.crypto_key
      ).toString();
      if (check_old == true) {
        let update_password = await db.users.update(
          { password: Newpassword },
          {
            where: {
              id: session.id,
            },
          }
        );
        req.flash("success", "Update password succesfully");
        res.redirect("/serviceprovider/login");
      } else {
        req.flash("error", "Please enter valid old password");
        res.redirect("/serviceprovider/changepasswordpage");
      }
    } catch (error) {
      return helper.error(res, error);
    }
  },
  logout: async (req, res) => {
    try {
      req.session.destroy();
      res.redirect("/serviceprovider/login");
    } catch (error) {
      return helper.error(res, error);
    }
  },
};
