const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); ////////////////
const ENV = process.env;
const sequelize = db.sequelize

const multer = require('multer');
const fs = require('fs');

const XLSX = require('xlsx');
const path = require('path');

var title = "subadmin_user";

db.subadmin_users.belongsTo(db.users, { foreignKey: 'subadmin_id' });

module.exports = {

    maid_listing: async (req, res) => {
        try {
            if (!req.session.admin) return res.redirect("/admin/login");
            const find_user = await db.subadmin_users.findAll({
                include: [{
                    model: db.users
                }],
                where: {
                    deletedAt: null,
                    usertype: 1
                },
                raw: true,
                nest: true,
                order: [['id', 'DESC']]

            });
            let session = req.session.admin;
            res.render("Admin/subadmin_users/maid_list", { session, find_user, title });
        } catch (error) {
            return helper.error(res, error);
        }
    },

    chef_listing: async (req, res) => {
        try {
            if (!req.session.admin) return res.redirect("/admin/login");
            const find_user = await db.subadmin_users.findAll({
                include: [{
                    model: db.users
                }],
                where: {
                    deletedAt: null,
                    usertype: 2
                },

                raw: true,
                nest: true,
                order: [['id', 'DESC']]

            });
            let session = req.session.admin;
            res.render("Admin/subadmin_users/chef_list", { session, find_user, title });
        } catch (error) {
            return helper.error(res, error);
        }
    },
    driver_listing: async (req, res) => {
        try {
            if (!req.session.admin) return res.redirect("/admin/login");
            const find_user = await db.subadmin_users.findAll({
                include: [{
                    model: db.users
                }],
                where: {
                    deletedAt: null,
                    usertype: 3
                },
                raw: true,
                nest: true,
                order: [['id', 'DESC']]

            });
            let session = req.session.admin;
            res.render("Admin/subadmin_users/driver_list", { session, find_user, title });
        } catch (error) {
            return helper.error(res, error);
        }
    },


};






