const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); ////////////////
const ENV = process.env;
const sequelize = db.sequelize
const Op=sequelize

var title = "bookings";

db.bookings.belongsTo(db.users, { foreignKey: 'user_id', as: 'user_details' });
db.bookings.belongsTo(db.subadmin_users, { foreignKey: 'book_to_user_id', as: 'subadmin_users' });
// db.bookings.belongsTo(db.users, { foreignKey: 'book_to_user_id', as: 'service_provoders' });




module.exports = {

    booking_listing: async (req, res) => {
        try {
            if (!req.session.serviceprovider) return res.redirect("/serviceprovider/login");
            const find_user = await db.bookings.findAll({
                where: {
                    book_to_user_id: req.session.serviceprovider.id,
                    type:4

                },
                include: [{
                    model: db.users,
                    as:"user_details"

                }],
                raw:true,
                nest:true,
                order: [['id', 'DESC']]

            });
            let session = req.session.serviceprovider;
            res.render("serviceprovider/bookings/listing", { session, find_user, title });
        } catch (error) {
            return helper.error(res, error);
        }
    },
    status_accept_reject: async (req, res) => {
        try {
          const data2 = await db.bookings.update({
            status: req.body.value
          }, {
            where: {
              id: req.body.id
            }
          });
          return res.status(200).json(data2)
    
        } catch (error) {
          console.log(error, '===========error================')
        }
    },

    subadmin_booking_listing: async (req, res) => {
      try {
          if (!req.session.subadmin) return res.redirect("/subadmin/login");
          const find_user = await db.bookings.findAll({
            where: {
              subadmin_id:req.session.subadmin.id,
              // type: {
              //     [Op.not]: 1
              // }
          },
              include: [{
                  model: db.users,
                  as:"user_details"

              },{
                model:db.subadmin_users,
                as:"subadmin_users"
              }],
              raw:true,
              nest:true,
              order: [['id', 'DESC']]

          });
          console.log(find_user,'============find_user==============')
          let session = req.session.subadmin;
          res.render("subadmin/bookings/listing", { session, find_user, title });
      } catch (error) {
          return helper.error(res, error);
      }
  },

  subadmin_status_accept_reject: async (req, res) => {
    try {
      const data2 = await db.bookings.update({
        status: req.body.value,
      }, {
        where: {
          id: req.body.id
        }
      });
      if(req.body.value==2){
        const data2 = await db.bookings.update({
          pending_amount: 0,
        }, {
          where: {
            id: req.body.id
          }
        });
      }
      return res.status(200).json(data2)

    } catch (error) {
      console.log(error, '===========error================')
    }
},

admin_booking_listing: async (req, res) => {
  try {
      if (!req.session.admin) return res.redirect("/admin/login");
      const find_user = await db.bookings.findAll({
          include: [{
              model: db.users,
              as:"user_details"

          },
          
          // {
          //   models:db.subadmin_users,
          //   as:"subadmin_users"
          // },
          // {
          //   models:db.users,
          //   as:"service_provoders"
          // },
          
        ],
        attributes: {
          include: [
            [sequelize.literal('(SELECT name FROM users WHERE id = bookings.book_to_user_id limit 1)'), 'service_provider_name'],
            [sequelize.literal('(SELECT name FROM subadmin_users WHERE id = bookings.book_to_user_id limit 1)'), 'subadmin_user_name']


          ],
        },
          raw:true,
          nest:true,
          order: [['id', 'DESC']]

      });
      console.log(find_user,'============find_users==================')
      let session = req.session.admin;
      res.render("Admin/bookings/listing", { session, find_user, title });
  } catch (error) {
      return helper.error(res, error);
  }
},



};
