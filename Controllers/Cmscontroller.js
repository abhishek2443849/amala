
const helper = require('../helpers/helper')
var CryptoJS = require("crypto-js");
const db = require('../models')
const ENV = process.env

var title = "cms"
module.exports = {
    
    privacy_policy: async (req, res) => {
        try {
            if (!req.session.admin) return res.redirect("/admin/login");
            const privacy_policy = await db.contents.findOne({
                where:{
                    accessor: "privacyPolicy"
                } })
            let session = req.session.admin

          
            res.render('Admin/cms/privacypolicy', { session,title,privacy_policy})
        } catch (error) {
            return helper.error(res, error)
        }
    },
    AboutUs: async (req, res) => {
        try {
            if (!req.session.admin) return res.redirect("/admin/login");
            let session = req.session.admin
            const aboutus = await db.contents.findOne({
                where:{
                    accessor: "aboutUs"
                }
            })
            res.render('Admin/cms/aboutus', { session, title,aboutus })
        } catch (error) {
            return helper.error(res, error)

        }
    },
    termsConditions: async (req, res) => {
        try {
            if (!req.session.admin) return res.redirect("/admin/login");
            let session = req.session.admin
            const termsCondition = await db.contents.findOne({
                where:{
                    accessor: "termsAndConditions"
                }
            })
            res.render('Admin/cms/termsConditions', { session, title,termsCondition })
        } catch (error) {
            return helper.error(res, error)

        }
    },
    privacy_policy_update: async (req, res) => {
        try {
           
            const upadteprivacy = await db.contents.update( req.body,{
                where:{
                    accessor: "privacyPolicy"
                }
            })
            req.flash('success', 'Privacy Plicy us updated succesfully')
            res.redirect(`back`)   
        } catch (error) {

        }
    },
    AboutUs_update: async (req, res) => {
        try {
            
            const upadteaboutus = await db.contents.update( req.body,{
                where:{
                    accessor: "aboutUs"
                }
            })
            req.flash('success', 'About us updated succesfully')
            res.redirect(`back`)          
        } catch (error) {

        }
    },
    terms_update: async (req, res) => {
        try {
            console.log(req.body,'=============req body =========')
            const upadteterms = await db.contents.update( req.body,{
                where:{
                    accessor: "termsAndConditions"
                }
            })
            req.flash('success', 'Terms and conditions updated succesfully')
            res.redirect(`back`)    
        } catch (error) {

        }
    },
}