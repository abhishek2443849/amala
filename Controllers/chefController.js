const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); ////////////////
const ENV = process.env;
const sequelize = db.sequelize

const multer = require('multer');
const fs = require('fs');

const XLSX = require('xlsx');
const path = require('path');


var title = "chef";

// db.users.belongsTo(db.services, { foreignKey: 'service_provider_service_type_id', as:"services" });

module.exports = {

  uploadChef: async (req, res) => {
    try {
      let session = req.session.subadmin.id
      if (!req.files || !req.files.excelFile) {
        return res.status(400).send('No file uploaded.');
      }

      const excelFile = req.files.excelFile;

      const filePath = 'temp.xlsx';
      excelFile.mv(filePath, async (err) => {
        if (err) {
          console.error('Error saving file:', err);
          return res.status(500).send('Error saving file.');
        }
        try {
          const workbook = XLSX.readFile(filePath);
          const sheetName = workbook.SheetNames[0];
          const sheet = workbook.Sheets[sheetName];
          const data = XLSX.utils.sheet_to_json(sheet);
          console.log(data, '=============data==============')
          fs.unlinkSync(filePath);
          await handleChefData(data, req.session.subadmin.id);
          // res.send('File data uploaded successfully.');
          res.redirect('/subadmin/chef_listing')
        } catch (error) {
          console.error('Error handling file upload:', error);
          res.status(500).send('Error handling file upload.');
        }
      });
    } catch (error) {
      console.error('Error handling file upload:', error);
      res.status(500).send('Error handling file upload.');
    }
  },
  add_chef: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      let session = req.session.subadmin;

      const languages = await db.languages.findAll({
        where: {
          deletedAt: null,

        },
      });


      res.render("subadmin/chef/add", { session, title, languages });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  chef_post: async (req, res) => {
    try {
      if (req.files && req.files.image) {
        let folder = "admin"
        let image = await helper.fileUpload(req.files.image, folder);

        req.body.image = image;
      }

      if (req.files && req.files.cv) {
        let folder = "admin"
        let image = await helper.fileUpload(req.files.cv, folder);
        req.body.cv = image;
      }

      if (req.files && req.files.passport) {
        let folder = "admin"
        let image = await helper.fileUpload(req.files.passport, folder);
        req.body.passport = image;
      }

      const add_inst = await db.subadmin_users.create({
        subadmin_id:req.session.subadmin.id,
        usertype:2,
        name:req.body.name,
        image:req.body.image,
        nationality:req.body.nationality,
        age:req.body.age,
        experience:req.body.experience,
        salery:req.body.salery,
        degree:req.body.degree,
        height:req.body.height,
        weight:req.body.weight,
        availablity:req.body.availablity,
        passport:req.body.passport,
        cv:req.body.cv
      });

      const selectedLanguages = req.body.language_id;
      for (const languageId of selectedLanguages) {
        await db.subadmin_users_languages.create({
          subadmin_users_id: add_inst.id,
          language_id: languageId,
        });
      }

      req.flash('success', 'Chef added successfully')
      res.redirect("/subadmin/chef_listing")
    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      res.status(500).send("Internal Server Error");
    }
  },

  chef_listing: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      const find_user = await db.subadmin_users.findAll({
        where: {
          deletedAt: null,
          subadmin_id:req.session.subadmin.id,
          usertype:2


        },
        order: [['id', 'DESC']]

      });
      let session = req.session.subadmin;
      res.render("subadmin/chef/listing", { session, find_user, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  chef_view: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      let session = req.session.subadmin;
      const data_view = await db.subadmin_users.findOne({
        where: {
          id: req.params.id,
        },
      });


      const saveLanguages = await db.subadmin_users_languages.findAll({
        where: {
          subadmin_users_id: req?.params?.id,
        },
        raw: true
      });

      const savedLanguageIds = saveLanguages.map(item => item.language_id);

      const languages = await db.languages.findAll({
        where: {
          id: {
            [db.Sequelize.Op.in]: savedLanguageIds,
          },
          deletedAt: null,
        }
      });

      res.render("subadmin/chef/view", { data_view, session, languages, savedLanguageIds, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  chef_edit: async (req, res) => {
    try {
      if (!req.session.subadmin) return res.redirect("/subadmin/login");
      let session = req.session.subadmin;
      
      const data_view = await db.subadmin_users.findOne({
        where: {
          id: req.params.id,
        },
       
      });


      const languages = await db.languages.findAll({
        where: {
          deletedAt: null,
        }
      });

      const saveLanguages = await db.subadmin_users_languages.findAll({
        where: {
          subadmin_users_id: req?.params?.id,
        },
        raw: true
      });

      const savedLanguageIds = saveLanguages.map(item => item.language_id);


      res.render("subadmin/chef/edit", { data_view, languages, saveLanguages, savedLanguageIds, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  chef_update: async (req, res) => {
    try {

      if (req.files && req.files.image) {
        let folder = "admin"
        let image = await helper.fileUpload(req.files.image, folder);

        req.body.image = image;
      }

      if (req.files && req.files.cv) {
        let folder = "admin"
        let image = await helper.fileUpload(req.files.cv, folder);
        req.body.cv = image;
      }

      if (req.files && req.files.passport) {
        let folder = "admin"
        let image = await helper.fileUpload(req.files.passport, folder);
        req.body.passport = image;
      }


      const deleteLanguages = await db.subadmin_users_languages.destroy({
        where: {
          subadmin_users_id:req.params.id
        }
      })
      const updateUser = await db.subadmin_users.update(
        { 
          name:req.body.name,
          image:req.body.image,
          nationality:req.body.nationality,
          age:req.body.age,
          experience:req.body.experience,
          salery:req.body.salery,
          degree:req.body.degree,
          height:req.body.height,
          weight:req.body.weight,
          availablity:req.body.availablity,
          passport:req.body.passport,
          cv:req.body.cv
        },

        {
          where: {
            id: req.params.id,
          },
        }
      );

      const selectedLanguages = req.body.language_id;
      for (const languageId of selectedLanguages) {
        await db.subadmin_users_languages.create({
          subadmin_users_id: req.params.id,
          language_id: languageId,
        });
      }

      req.flash('success', 'Chef updated succesfully')
      res.redirect("/subadmin/chef_listing")
    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      // res.status(500).send("Internal Server Error");
    }
  },
  chef_status: async (req, res) => {
    try {
      const find_status = await db.subadmin_users.update(
        { status: req.body.value },
        {
          where: { id: req.body.id },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
  chef_deleted: async (req, res) => {
    try {
      let data = Date.now();
      const userdelete = await db.subadmin_users.update(
        { deletedAt: data },
        {
          where: {
            id: req.body.id,
          },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
};


async function handleChefData(data, subadminId) {
  try {
    for (const chef of data) {
      const existingchef = await db.subadmin_users.findOne({ where: { name: chef.name,deletedAt:null,usertype:2,subadmin_id:subadminId} });
      if (existingchef) {
        console.log(`chef with name '${chef.name}' already exists. Skipping insertion.`);
        continue; 
      }

      console.log('chef.language:', chef.language); 
      
      const age = chef.age.replace(/"/g, '');
      
      let selectedLanguages;
      if (typeof chef.language === 'string') {
        selectedLanguages = chef.language.split(',');
      } else {
        selectedLanguages = [chef.language];
      }

      const bulkupload = await db.subadmin_users.create({
        subadmin_id: subadminId,
        usertype:2,
        name: chef.name,
        nationality: chef.nationality,
        age: age,
        experience: chef.experience,
        salery: chef.salary,
        degree: chef.degree,
        height: chef.height,
        weight: chef.weight,
        availability: chef.availability,
        language: chef.language 
      });

      for (const language of selectedLanguages) {
        await db.subadmin_users_languages.create({
          subadmin_users_id: bulkupload.id,
          language_id: language, 
        });
      }
    }

    console.log('Data inserted into database successfully.');
  } catch (error) {
    console.error('Error inserting data into database:', error);
    throw error;
  }
}
