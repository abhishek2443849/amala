const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); ////////////////
const ENV = process.env;

var title = "languages";
module.exports = {
  add_languages: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;

      res.render("Admin/languages/add", { session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  languages_post: async (req, res) => {
    try {
      
      const add_inst = await db.languages.create({
        name: req.body.name,
     
      });

      req.flash('success', 'Languages add succesfully')
      res.redirect("/admin/languages_listing")
    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      res.status(500).send("Internal Server Error");
    }
  },

  languages_listing: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      const find_user = await db.languages.findAll({
        where: {
          deletedAt: null,
        },
        order: [['id', 'DESC']]

      });
      let session = req.session.admin;
      res.render("Admin/languages/listing", { session, find_user, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  languages_view: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      const data_view = await db.languages.findOne({
        where: {
          id: req.params.id,
        },
      });
      res.render("Admin/languages/view", { data_view, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  languages_edit: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      const data_view = await db.languages.findOne({
        where: {
          id: req.params.id,
        },
      });
      res.render("Admin/languages/edit", { data_view, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  languages_update: async (req, res) => {
    try {
      
      const updateUser = await db.languages.update(
        req.body,

        {
          where: {
            id: req.params.id,
          },
        }
      );

      req.flash('success', 'Languages updated succesfully')
      res.redirect("/admin/languages_listing")
    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      // res.status(500).send("Internal Server Error");
    }
  },
  languages_status: async (req, res) => {
    try {
      const find_status = await db.languages.update(
        { status: req.body.value },
        {
          where: { id: req.body.id },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
  languages_deleted: async (req, res) => {
    try {
      let data = Date.now();
      const userdelete = await db.languages.update(
        { deletedAt: data },
        {
          where: {
            id: req.body.id,
          },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
};
