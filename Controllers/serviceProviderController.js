const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); ////////////////
const ENV = process.env;
const sequelize = db.sequelize

var title = "serviceprovider";

// db.users.belongsTo(db.services, { foreignKey: 'service_provider_service_type_id', as:"services" });

module.exports = {
  add_service_provider: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;

      const services = await db.services.findAll({});
      const languages = await db.languages.findAll({
        where: {
          deletedAt: null,

        },
      });


      res.render("Admin/service_provider/add", { session, title, services, languages });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  check_serviceprovider_email: async (req, res) => {
    try {
      const data = await db.users.findAll({
        where: {
          email: req.query.email,
          role:'3',
          deletedAt:null
        }
      })
      if (data.length > 0) {
        return res.json(false)
      }
      else {
        return res.json(true)
      }
    } catch (error) {
      console.log(error, '========================error===================')

    }
  },

  service_provider_post: async (req, res) => {
    try {

      if (req.files && req.files.image) {
        let folder = "admin"
        let image = await helper.fileUpload(req.files.image, folder);

        req.body.image = image;
      }

      var data = +req.body.password;
      var Newpassword = CryptoJS.AES.encrypt(
        JSON.stringify(data),
        ENV.crypto_key
      ).toString();
      const add_inst = await db.users.create({
        name: req.body.name,
        email: req.body.email,
        image: req.body.image,
        country_code:req.body.country_code,
        phone: req.body.phone,
        role: req.body.role,
        password: Newpassword,
        experience: req.body.experience,
        nationality: req.body.nationality,
        service_provider_service_type_id: req.body.service_provider_service_type_id,
        salery:req.body.salery,
        availablity:req.body.availablity,
        status:'1'

      });

      const selectedLanguages = req.body.language_id;
      for (const languageId of selectedLanguages) {
        await db.service_provider_languages.create({
          service_provider_id: add_inst.id,
          language_id: languageId,
        });
      }

      const selectedServices = req.body.service_provider_service_type_id;
      for (const languageId of selectedServices) {
        await db.service_provider_service_types.create({
          service_provider_id: add_inst.id,
          service_id: languageId,
        });
      }

      req.flash('success', 'Service provider add succesfully')
      res.redirect("/admin/service_provider_listing")
    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      res.status(500).send("Internal Server Error");
    }
  },

  service_provider_listing: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      const find_user = await db.users.findAll({
        where: {
          deletedAt: null,
          role: "3"
        },
        order: [['id', 'DESC']]

      });
      let session = req.session.admin;
      res.render("Admin/service_provider/listing", { session, find_user, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  service_provider_view: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      const data_view = await db.users.findOne({
        where: {
          id: req.params.id,
        },
        attributes: {
          include: [
            [sequelize.literal('(SELECT name FROM services WHERE id = users.service_provider_service_type_id)'), 'service_type_name']

          ],
        },
        raw: true,
        nest: true

      });


      const saveLanguages = await db.service_provider_languages.findAll({
        where: {
          service_provider_id: req?.params?.id,
        },
        raw: true
      });

      const savedLanguageIds = saveLanguages.map(item => item.language_id);

      const languages = await db.languages.findAll({
        where: {
          id: {
            [db.Sequelize.Op.in]: savedLanguageIds,
          },
          deletedAt: null,
        }
      });


      const services = await db.services.findAll({
        where: {
          deletedAt: null,
        }
      });

      const saveServiceType = await db.service_provider_service_types.findAll({
        where: {
          service_provider_id: req?.params?.id,
        },
        raw: true
      });

      const savedserviceProviderIds = saveServiceType.map(item => item.service_id);

      res.render("Admin/service_provider/view", { data_view, session, languages, savedLanguageIds,services,savedserviceProviderIds, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  service_provider_edit: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      const data_view = await db.users.findOne({
        where: {
          id: req.params.id,
        },
        attributes: {
          include: [
            [sequelize.literal('(SELECT name FROM services WHERE id = users.service_provider_service_type_id)'), 'service_type_name']

          ],
        },
      });

      // const services = await db.services.findAll({});

      const languages = await db.languages.findAll({
        where: {
          deletedAt: null,
        }
      });
      const saveLanguages = await db.service_provider_languages.findAll({
        where: {
          service_provider_id: req?.params?.id,
        },
        raw: true
      });

      const savedLanguageIds = saveLanguages.map(item => item.language_id);



      const services = await db.services.findAll({
        where: {
          deletedAt: null,
        }
      });

      const saveServiceType = await db.service_provider_service_types.findAll({
        where: {
          service_provider_id: req?.params?.id,
        },
        raw: true
      });

      const savedserviceProviderIds = saveServiceType.map(item => item.service_id);

      // const saveSerivces = await db.services.findAll({
      //   where: {
      //     service_provider_id: req?.params?.id,
      //   },
      //   raw: true

      // });


      res.render("Admin/service_provider/edit", { data_view, languages, services, saveLanguages, savedLanguageIds,saveServiceType,savedserviceProviderIds, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  service_provider_update: async (req, res) => {
    try {



      if (req.files && req.files.image) {
        let folder = "admin"
        let images = await helper.fileUpload(req.files.image, folder);

        req.body.image = images;
      }

      const deleteLanguages = await db.service_provider_languages.destroy({
        where: {
          service_provider_id:req.params.id
        }
      })

      const deleteServices = await db.service_provider_service_types.destroy({
        where: {
          service_provider_id:req.params.id
          
        }
      })
      const updateUser = await db.users.update(
        req.body,

        {
          where: {
            id: req.params.id,
          },
        }
      );

      const selectedLanguages = req.body.language_id;
      for (const languageId of selectedLanguages) {
        await db.service_provider_languages.create({
          service_provider_id: req.params.id,
          language_id: languageId,
        });
      }

      const selectedServices = req.body.service_provider_service_type_id;
      for (const languageId of selectedServices) {
        await db.service_provider_service_types.create({
          service_provider_id: req.params.id,
          service_id: languageId,
        });
      }

      req.flash('success', 'Service provider updated succesfully')
      res.redirect("/admin/service_provider_listing")
    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      // res.status(500).send("Internal Server Error");
    }
  },
  service_provider_status: async (req, res) => {
    try {
      const find_status = await db.users.update(
        { status: req.body.value },
        {
          where: { id: req.body.id },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
  service_provider_deleted: async (req, res) => {
    try {
      let data = Date.now();
      const userdelete = await db.users.update(
        { deletedAt: data },
        {
          where: {
            id: req.body.id,
          },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
};
