const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); 
const ENV = process.env;

var title = "servicefees";
module.exports = {
  add_services_fees: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;

      const fees = await db.admin_service_fees.findOne({});
      res.render("Admin/services_fees/edit", { session,fees,title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  services_fees_update: async (req, res) => {
    try {
   
      const existingFees = await db.admin_service_fees.findOne();

    if (existingFees) {
      await db.admin_service_fees.update(
        { fees: req.body.fees },
        {
          where: {
            id:existingFees.id
          },
        }
      );
    } else {
      await db.admin_service_fees.create({
        fees: req.body.fees,
      });
    }
      req.flash('success', 'Services fees updated succesfully')
      res.redirect("/admin/add_services_fees")
    } catch (error) {
      console.error("Error updating user:", error);
      res.status(500).send("Internal Server Error");
    }
  },
};
