
const helper = require('../helpers/helper')
var CryptoJS = require("crypto-js");
const db = require('../models')
const ENV = process.env

var title = "subadmincms"
module.exports = {
    
    privacy_policy: async (req, res) => {
        try {
            if (!req.session.subadmin) return res.redirect("/subadmin/login");
            const privacy_policy = await db.contents.findOne({
                where:{
                    accessor: "privacyPolicy"
                } })
            let session = req.session.subadmin

          
            res.render('subadmin/cms/privacypolicy', { session,title,privacy_policy})
        } catch (error) {
            return helper.error(res, error)
        }
    },
    AboutUs: async (req, res) => {
        try {
            if (!req.session.subadmin) return res.redirect("/subadmin/login");
            let session = req.session.subadmin
            const aboutus = await db.contents.findOne({
                where:{
                    accessor: "aboutUs"
                }
            })
            res.render('subadmin/cms/aboutus', { session, title,aboutus })
        } catch (error) {
            return helper.error(res, error)

        }
    },
    termsConditions: async (req, res) => {
        try {
            if (!req.session.subadmin) return res.redirect("/subadmin/login");
            let session = req.session.subadmin
            const termsCondition = await db.contents.findOne({
                where:{
                    accessor: "termsAndConditions"
                }
            })
            res.render('subadmin/cms/termsConditions', { session, title,termsCondition })
        } catch (error) {
            return helper.error(res, error)

        }
    },
 
}