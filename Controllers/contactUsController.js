const db = require('../models');


// Notifcations.belongsTo(User,{foreignKey:'recieverId'});


module.exports = {
    contactUs : async (req ,res ) => {
    try{
        if (!req.session.admin) return res.redirect("/admin/login");
        let contactUs = await db.contact_us.findAll({
            order: [
                ['id' , 'DESC']
            ]
        });

      
        let session = req.session.admin;
        res.render('Admin/contactus/listing',{
            title: 'contact_us',
            contactUs,
            session
        });  
    }
    catch(err){
        throw err;
    }      
},


  
}