const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); ////////////////
const ENV = process.env;
const sequelize = db.sequelize
const Op=sequelize

var title = "transactions";

// db.bookings.belongsTo(db.users, { foreignKey: 'user_id', as: 'user_details' });





module.exports = {

admin_transactions_listing: async (req, res) => {
  try {
      if (!req.session.admin) return res.redirect("/admin/login");
      const find_user = await db.bookings.findAll({
          include: [{
              model: db.users,
              as:"user_details"

          },
          
          // {
          //   models:db.subadmin_users,
          //   as:"subadmin_users"
          // },
          // {
          //   models:db.users,
          //   as:"service_provoders"
          // },
          
        ],
        attributes: {
          include: [
            [sequelize.literal('(SELECT name FROM users WHERE id = bookings.book_to_user_id limit 1)'), 'service_provider_name'],
            [sequelize.literal('(SELECT name FROM subadmin_users WHERE id = bookings.book_to_user_id limit 1)'), 'subadmin_user_name']


          ],
        },
          raw:true,
          nest:true,
          order: [['id', 'DESC']]

      });
      let session = req.session.admin;
      res.render("Admin/transactions/listing", { session, find_user, title });
  } catch (error) {
      return helper.error(res, error);
  }
},



};
