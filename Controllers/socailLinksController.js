const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); 
const ENV = process.env;

var title = "social_links";
module.exports = {
  add_socail_links: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;

      const fees = await db.social_links.findOne({});
      res.render("Admin/social_links/edit", { session,fees,title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  socail_links_update: async (req, res) => {
    try {
   
      const existingFees = await db.social_links.findOne();

    if (existingFees) {
      await db.social_links.update(
        req.body,
        {
          where: {
            id:existingFees.id
          },
        }
      );
    } else {
      await db.social_links.create(
        req.body
      );
    }
      req.flash('success', 'Links updated succesfully')
      res.redirect("/admin/add_socail_links")
    } catch (error) {
      console.error("Error updating user:", error);
      res.status(500).send("Internal Server Error");
    }
  },
};
