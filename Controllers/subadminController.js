const helper = require("../helpers/helper");
var CryptoJS = require("crypto-js");
const db = require("../models"); ////////////////
const ENV = process.env;

var title = "subadmin";
module.exports = {
  add_subadmin: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;

      res.render("Admin/subadmin/add", { session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  check_email: async (req, res) => {
    try {
      const data = await db.users.findAll({
        where: {
          email: req.query.email,
          role:'2',
          deletedAt:null
        }
      })
      if (data.length > 0) {
        return res.json(false)
      }
      else {
        return res.json(true)
      }
    } catch (error) {
      console.log(error, '========================error===================')

    }
  },

  check_username: async (req, res) => {
    try {
      const data = await db.users.findAll({
        where: {
          username: req.query.username,
          role:'2',
          deletedAt:null
        }
      })
      if (data.length > 0) {
        return res.json(false)
      }
      else {
        return res.json(true)
      }


    } catch (error) {
      console.log(error, '========================error===================')

    }
  },



  subadminpost: async (req, res) => {
    try {
      console.log(req.body,'=============req body===============')
      if (req.files && req.files.image) {
        let folder = "admin"
        let image = await helper.fileUpload(req.files.image, folder);

        req.body.image = image;
      }

      var data = +req.body.password;
      var Newpassword = CryptoJS.AES.encrypt(
        JSON.stringify(data),
        ENV.crypto_key
      ).toString();
      const add_inst = await db.users.create({
        name: req.body.name,
        email:req.body.email,
        username:req.body.username,
        image: req.body.image,
        role:req.body.role,
        address:req.body.address,
        password:Newpassword,
        status:'1'
      });

      req.flash('success', 'Subadmin add succesfully')
      res.redirect("/admin/subadminlisting")
    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      res.status(500).send("Internal Server Error");
    }
  },

  interestlisting: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      const find_user = await db.users.findAll({
        where: {
          deletedAt: null,
          role: "2"
        },
        order: [['id', 'DESC']]

      });
      let session = req.session.admin;
      res.render("Admin/subadmin/listing", { session, find_user, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },
  interests_view: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      const data_view = await db.users.findOne({
        where: {
          id: req.params.id,
        },
      });
      res.render("Admin/subadmin/view", { data_view, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  interests_edit: async (req, res) => {
    try {
      if (!req.session.admin) return res.redirect("/admin/login");
      let session = req.session.admin;
      const data_view = await db.users.findOne({
        where: {
          id: req.params.id,
        },
      });
      res.render("Admin/subadmin/edit", { data_view, session, title });
    } catch (error) {
      return helper.error(res, error);
    }
  },

  interest_update: async (req, res) => {
    try {
      if (req.files && req.files.image) {
        let folder = "admin"
        let images = await helper.fileUpload(req.files.image, folder);

        req.body.image = images;
      }
      const updateUser = await db.users.update(
        req.body,

        {
          where: {
            id: req.params.id,
          },
        }
      );
      req.flash('success', 'Subadmin updated succesfully')
      res.redirect("/admin/subadminlisting");

    } catch (error) {
      // Handle errors appropriately
      console.error("Error updating user:", error);
      // res.status(500).send("Internal Server Error");
    }
  },
  interest_status: async (req, res) => {
    try {
      const find_status = await db.users.update(
        { status: req.body.value },
        {
          where: { id: req.body.id },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
  interest_deleted: async (req, res) => {
    try {
      let data = Date.now();
      const userdelete = await db.users.update(
        { deletedAt: data },
        {
          where: {
            id: req.body.id,
          },
        }
      );
      res.send(true);
    } catch (error) {
      return helper.error(res, error);
    }
  },
};
