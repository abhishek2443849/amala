
const helper = require('../helpers/helper')
var CryptoJS = require("crypto-js");
const db = require('../models')////////////////
const ENV = process.env

var title = "users"
module.exports = {
    userlisting: async (req, res) => {
        try {
            if (!req.session.admin) return res.redirect("/admin/login");
            const find_user = await db.users.findAll({
                where:{
                    deletedAt: null , 
                    role: "1"
                } ,
                order: [['id', 'DESC']]

            })
            let session = req.session.admin
            res.render('Admin/users/listing', { session, find_user, title })
        } catch (error) {
            return helper.error(res, error)
        }
    },
    userview: async (req, res) => {
        try {
            if (!req.session.admin) return res.redirect("/admin/login");
            let session = req.session.admin
            const userview = await db.users.findOne({where:{
                id: req.params.id
            }  })
            res.render('Admin/users/view', { userview, session, title })
        } catch (error) {
            return helper.error(res, error)

        }
    },

    viewupdate: async (req, res) => {
    try {
        const updateUser = await db.users.update(
         
            req.body,
          
            {
                where: {
                    id: req.params.id
                }
            }
        );

        res.redirect('back');
    } catch (error) {
        // Handle errors appropriately
        console.error('Error updating user:', error);
        res.status(500).send('Internal Server Error');
    }
},
    user_status: async (req, res) => {
        try {
            const find_status = await db.users.update(
                { status: req.body.value },{
                where:
                {id: req.body.id }})
            res.send(true)
        } catch (error) {
            return helper.error(res, error)
        }
    },
    deleteduser: async (req, res) => {
        try {
            let data=Date.now()
            const userdelete = await db.users.update({ deletedAt: data },
                {
                    where:{
                id: req.body.id 
            } }, )
            res.send(true)
        } catch (error) {
            return helper.error(res, error)
        }
    },
}