const db = require('../models');
const User = db.users;
const Notifcations = db.notifications;


Notifcations.belongsTo(User,{foreignKey:'recieverId'});
Notifcations.belongsTo(User,{foreignKey:'senderId' ,as:"senderDetail"});

module.exports = {
    notification : async (req ,res ) => {
    try{
        if (!req.session.admin) return res.redirect("/admin/login");
        // console.log("req.params.page", req.params.page)
        console.log("req.query.type",req.query.type)
        let notificationuser = await User.findAll({
            attributes:['id','name'],
            where:{
                role:req.query.type
            },
          
            order: [
                ['id' , 'DESC']
            ]
        });

        let AllNotifications= await Notifcations.findAll({
            include: [
                {
                    model: User,
                    where: {
                        role: req.query.type,
                    },
                }
            ],
            where:{
                deletedAt:null

            },
            raw:true,
            nest:true,
            order: [
                ["id", "DESC"]
              ]

        })
        let session = req.session.admin;
        let type = req.query.type;
        res.render('Admin/notification/index',{
            title: 'notification',
            notificationuser,
            AllNotifications,
            type,
            session
        });  
    }
    catch(err){
        throw err;
    }      
},

// store:async(req,res)=>{
//     try {
       
      
      
//     let notiArray = []
//     let recieverId = req.body.recieverId;
//     console.log(recieverId,'==================receverid==============')
//     for(let i in recieverId){
//         let obj = {}
//         obj.recieverId = recieverId[i];
//         obj.senderId = 1;
//         obj.message = req.body.message;
//         notiArray.push(obj)
//     }
//     // console.log("notiArray", notiArray)
//     // return
//     await Notifcations.bulkCreate(notiArray); 
//     req.flash('success', 'Notification sent Successfully!')
//     res.redirect('/admin/notification/users?type='+req.query.type);
//    } catch (error) {
//     console.log(error);
//    }
   
    
// },

store: async (req, res) => {
    try {
        let recieverIds = req.body.recieverId;
        
        // Check if recieverIds is an array or a single value
        if (Array.isArray(recieverIds)) {
            // Multiple receiver IDs
            let notiArray = recieverIds.map(id => ({
                recieverId: id,
                senderId: 1,
                message: req.body.message
            }));
            
            await Notifcations.bulkCreate(notiArray);
        } else {
            // Single receiver ID
            let obj = {
                recieverId: recieverIds,
                senderId: 1,
                message: req.body.message
            };
            
            await Notifcations.create(obj);
        }
        
        req.flash('success', 'Notification sent Successfully!');
        res.redirect('/admin/notification/users?type=' + req.query.type);
    } catch (error) {
        console.log(error);
    }
},


subadmin_notification : async (req ,res ) => {
    try{
        if (!req.session.subadmin) return res.redirect("/subadmin/login");
    
        let AllNotifications= await Notifcations.findAll({
            where:{
                recieverId:req.session.subadmin.id
            },
            include: [
                {
                    model: User,
                    as:"senderDetail"
                    
                }
            ],
            raw:true,
            nest:true,
            order: [
                ["id", "DESC"]
              ]

        })
        let session = req.session.subadmin;
        let type = req.query.type;
        res.render('subadmin/notification/index',{
            title: 'notification',
            AllNotifications,
            session
        });  
    }
    catch(err){
        throw err;
    }      
},


  
}